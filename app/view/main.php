<?php
/**
 * Created by Vitaly Iegorov <egorov@samsonos.com>
 * on 12.06.14 at 14:43
 */
?>
<form class="converter" action="<?php url_base('main', 'html2less')?>" method="post">
    <div class="input source">
        <textarea class="input" name="source" placeholder="Source HTML code"><?php vi('source')?></textarea>
        <div class="controls">
            <a class="btn info" title="Click to see help">help me</a><a class="btn upload hidden">upload</a><a class="btn submit">convert</a><a class="description">HTML</a>
        </div>

    </div>
    <div class="input less">
        <textarea class="output" name="output" placeholder="Output LESS code"><?php vi('less')?></textarea>
        <div class="controls">
            <a class="description">LESS</a><a class="btn selectall hidden" href="">select all</a><a class="btn download hidden" target="_blank" href="">download</a>
        </div>
    </div>
    <input style="display:none" id="submit" type="submit" class="hidden">
</form>
<div class="info-block">
    <div class="close-button">X</div>
    <h1>HTML to LESS transformer v 1.0</h1>
    <div class="description">
        The main goal of this service is to simplify the routine process of HTML markup creation. Now you don't need to create LESS files for your
        HTML markup by hand - use html2less transformer. Also this service is absolutely free!
    </div>
    <br>
    <div class="description">
        <h2>Usage:</h2>
        <ul>
            <li>You can drop your files containing HTML markup into HTML editor and system will automatically convert it to LESS code.</li>
            <li>You can copy past or write your HTML code and press "convert" button to transform HTML code to LESS code</li>
            <li>After HTML to LESS conversion is finished you can:
                <ul>
                    <li>Click "download" button to load generated LESS file</li>
                    <li>Click "select all" then right mouse button -> copy generated LESS file</li>
                </ul>
            </li>
        </ul>
    </div>
    <br>
    <div class="description">
        This online service is based on SamsonPHP <a target="_blank" href="https://github.com/samsonos/php_skeleton">Skeleton module.</a>
    </div>
    <br>
    <div class="block">If you have found any bugs or would like to suggest some improvements to the project you can make it <a target="blank" href="https://github.com/samsonos/php_skeleton/issues">here</a>.</div>
    <br>
    <div class="copyright2">©<?php echo date('Y')?> SamsonOS</div>
</div>