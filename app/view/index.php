<html>
    <head>
        <title>HTML to LESS converter - Skeleton SamsonPHP module</title>
        <link rel="stylesheet" href="http://codemirror.net/lib/codemirror.css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,cyrillic-ext" rel="stylesheet">
        <script src="http://codemirror.net/lib/codemirror.js"></script>
        <script src="http://codemirror.net/addon/edit/matchbrackets.js"></script>
        <script src="http://codemirror.net/mode/xml/xml.js"></script>
        <script src="http://codemirror.net/mode/css/css.js"></script>
        <script src="http://codemirror.net/mode/javascript/javascript.js"></script>
        <script src="http://codemirror.net/mode/htmlmixed/htmlmixed.js"></script>
    </head>
    <body>
        <section class="body"><?php m()->render()?></section>
        <div class="overlay"><?php echo m()->output('loader')?></div>
        <div class="copyright">Developed by <a target="_blank" href="http://samsonos.com">SamsonOS</a></div>

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-12528485-9', 'html2less.com');
            ga('send', 'pageview');

        </script>
    </body>
</html>