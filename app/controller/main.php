<?php
/**
 * Created by Vitaly Iegorov <egorov@samsonos.com>
 * on 12.06.14 at 14:40
 */

/** Universal main controller */
function main__HANDLER()
{
    m()->view('main');
}

/**
 * Asynchronous HTML to LESS converter
 * @return array Asynchronous response array
 */
function main_async_html2less()
{
    $result = array('status' => '0');
    if(isset($_POST['source'])) {
        // Save last source
        $_SESSION['_skeleton_less_source'] = $_POST['source'];

        // Build LESS node tree
        $lessTree = new \samsonos\php\skeleton\Tree();

        // Convert to LESS
        $result['less'] = $lessTree->toLESS($_POST['source']);
        // Generate hash file name
        $result['file'] = md5($result['less']);

        // Save file
        file_put_contents('upload/'.$result['file'].'.less', $result['less']);

        $result['status'] = '1';
    }

    return $result;
}

/**
 * Controller for serving generated LESS files
 * @param string $hash LESS hashed identifier
 */
function main_download($hash = null)
{
    $file = $hash.'.less';
    if (file_exists('upload/'.$file)) {
        header('Content-type: text/less');
        header('Content-Disposition: attachment; filename="'.$file.'"');
        readfile('upload/'.$file);
    } else {
        return A_FAILED;
    }
}
