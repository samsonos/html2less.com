/**
 * Created by Vitaly Egorov <egorov@samsonos.com> on 12.06.14.
 */
s(document).pageInit(function(){

    var loader = s('.overlay');
    var download = s('.download');
    var selectall = s('.selectall');
    var filename = '';

    // Bind HTML editor
    var ta = document.getElementsByName('source');
    var hm = CodeMirror.fromTextArea(ta[0], {
        lineNumbers: true,
        mode: "htmlmixed"
    });

    var source = s('.source');
    hm.on('drop', function(cm, event){
        loader.show();

        setTimeout(function(){
            s('#submit').click();// Convert HTML
            hm.doc.setCursor({'line':0, 'ch':0}); // Deselect inserted text
        }, 1000);
    });

    // Bind LESS editor
    var ta2 = document.getElementsByName('output');
    var lm = CodeMirror.fromTextArea(ta2[0], {
        lineNumbers: true,
        matchBrackets : true,
        mode: "text/x-less"
    });

    var converter = s('.converter');
    // Bind asynchronous form send
    converter.ajaxSubmit(function(response){
        // If we have received converted LESS
        loader.hide();
        if(typeof response.less != 'undefined' && response.less.length) {
            // Show it
            lm.setValue(response.less);

            // Show controls
            selectall.removeClass('hidden');
            download.removeClass('hidden');
            download.a('href', '/main/download/'+response.file)
        }
    }, function(){
        loader.show();
        return true;
    });

    // Imitate form submit
    s('.submit').click(function(){
        s('#submit').click();
    },true, true);

    // Bind info popup
    s('.info').click(function(){
        tinybox(s('.info-block'));
    });

    // Bind LESS Code selection for copy
    selectall.click(function(){
        lm.execCommand('selectAll');
    }, true, true);
});